main :: IO ()
main = do
    fileContent <- readFile "./users.txt"
    let fileLines = lines fileContent
        withNumTuple = fileLines `zip` [0..]
        withNumStr = map (\(line, num) -> (num : line)) numberedTuple
    mapM_ putStrLn withNumStr 